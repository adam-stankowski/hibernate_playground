package as.hibernate.service;

import as.hibernate.model.Item;
import as.hibernate.model.ItemBag;
import as.hibernate.persistence.PersistenceService;
import org.junit.Before;
import org.junit.Test;

public class ItemCollectionServiceTest {

    private ItemCollectionService itemCollectionService = new ItemCollectionService();

    @Before
    public void setUp() {
    }

    @Test
    public void testPersist() {
        Item item = new Item();
        item.setName("Cupboard");
        item.addImage("cupboard.png");
        item.addImage("cupboard_fromt.png");
        itemCollectionService.persistItem(item);
    }

    @Test
    public void testAmend() {
        itemCollectionService.amendItem(1L, "Hello.png");
    }

    @Test
    public void testRemove() {
        itemCollectionService.removeItem(1L);
    }

    @Test
    public void testPersistBag() {
        ItemBag itemBag = new ItemBag();
        itemBag.setName("Cupboard");
        itemBag.addImage("cupboard.png");
        itemBag.addImage("cupboard_fromt.png");
        itemCollectionService.persistItem(itemBag);
    }

    @Test
    public void testAmendBag() {
        PersistenceService.executeInTransaction(em -> {
            ItemBag itemBag = em.find(ItemBag.class, 1L);
            itemBag.addImage("Hello.png");
        });
    }
}