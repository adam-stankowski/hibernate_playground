package as.hibernate.service;

import as.hibernate.model.Image;
import as.hibernate.model.ItemEmbeddableMap;
import as.hibernate.model.ItemWithImageEmbeddable;
import org.junit.Test;

public class ItemEmbeddableServiceTest {
    private ItemEmbeddableService itemEmbeddableService = new ItemEmbeddableService();

    @Test
    public void testPersist() {
        Image image = new Image("Picasso", "picasso.png");
        ItemWithImageEmbeddable item = new ItemWithImageEmbeddable();
        item.setName("Desk");
        item.addImage(image);
        itemEmbeddableService.persist(item);
    }

    @Test
    public void testAmend(){
        Image image = new Image("Winnie The Pooh", "winnie_the_pooh.png");
        itemEmbeddableService.addImage(1L, image);
    }

    @Test
    public void testPersistMap() {
        Image image = new Image("Picasso", "picasso.png");
        ItemEmbeddableMap item = new ItemEmbeddableMap();
        item.setName("Table");
        item.putImage("nice painting", image);
        itemEmbeddableService.persist(item);
    }

    @Test
    public void testAmentMap() {
        Image image = new Image("Matejko", "Matejko.png");
        itemEmbeddableService.putImageInMap(1L, "another nice painting", image);
    }

}