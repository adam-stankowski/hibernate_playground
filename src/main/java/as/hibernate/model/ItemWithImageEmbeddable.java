package as.hibernate.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Table(name = "item_image_embedable")
@Entity
public class ItemWithImageEmbeddable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    @ElementCollection
    @CollectionTable(name = "item_image_embeddable_images", joinColumns = @JoinColumn(name = "item_with_image_embeddable_id"))
    private Set<Image> images = new HashSet<>();

    public Long getId() {
        return id;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Image> getImages() {
        return images;
    }

    public void addImage(Image image) {
        this.images.add(image);
    }
}
