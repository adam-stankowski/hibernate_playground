package as.hibernate.model;

import org.hibernate.annotations.CollectionId;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

/**
 * This class is a modification of item with a Set, though now you can keep duplicates in Images
 * due to additional column image_id which makes all elements unique. images can now hold duplicates
 * ps. It may be hard to re-create this mapping from schema. huh...
 */
@Entity
public class ItemBag {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    @ElementCollection
    @CollectionTable(name = "itembag_images")
    @Column(name = "filename")
    @CollectionId(columns = @Column(name = "image_id"), type = @Type(type = "long"),
            generator = "images_id_generator")
    @TableGenerator(name = "images_id_generator", schema = "hibernate_playground",
    table = "images_id_generator", pkColumnName = "pk", valueColumnName = "value")
    private Collection<String> images = new ArrayList<String>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public Collection<String> getImages() {
        return images;
    }

    public void addImage(String image) {
        images.add(image);
    }
}
