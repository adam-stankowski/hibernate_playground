package as.hibernate.model;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;

@Entity
@Table(name = "item_embeddable_map")
public class ItemEmbeddableMap {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @ElementCollection
    @CollectionTable(name = "item_embeddable_map_image")
    @MapKeyColumn(name = "filename")
    private Map<String, Image> imageMap = new HashMap<>();

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void putImage(String key, Image image) {
        this.imageMap.put(key, image);
    }
}
