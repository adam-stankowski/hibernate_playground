package as.hibernate.service;

import as.hibernate.model.Item;
import as.hibernate.persistence.PersistenceService;

class ItemCollectionService {
    void persistItem(Object item) {
        PersistenceService.executeInTransaction(em -> em.persist(item));
    }

    void amendItem(Long id, String imageName) {
        PersistenceService.executeInTransaction(em -> {
            Item item = em.find(Item.class, id);
            item.addImage(imageName);
            em.merge(item);
        });
    }

    void removeItem(Long id) {
        PersistenceService.executeInTransaction(entityManager -> {
            Item item = entityManager.find(Item.class, id);
            entityManager.remove(item);
        });
    }
}
