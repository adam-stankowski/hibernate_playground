package as.hibernate.service;

import as.hibernate.model.Image;
import as.hibernate.model.ItemEmbeddableMap;
import as.hibernate.model.ItemWithImageEmbeddable;
import as.hibernate.persistence.PersistenceService;

class ItemEmbeddableService {

    void persist(Object item) {
        PersistenceService.executeInTransaction(em -> em.persist(item));
    }

    void addImage(Long itemId, Image image) {
        PersistenceService.executeInTransaction(em -> {
            ItemWithImageEmbeddable item = em.find(ItemWithImageEmbeddable.class, itemId);
            item.addImage(image);
        });
    }

    void putImageInMap(Long itemId, String key, Image image){
        PersistenceService.executeInTransaction(em -> {
            ItemEmbeddableMap item = em.find(ItemEmbeddableMap.class, itemId);
            item.putImage(key, image);
        });
    }
}
